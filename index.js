// Write a NodeJS application that exports a function that can
// retrieve the contents (filenames/dirnames) of a given target path
// and all its subdirectories. The function should return a
// JavaScript object with 2 array properties. Given the file structure below:

// {
//   filenames: [
//     "foo/f1.txt",
//     "foo/f2.txt",
//     "foo/bar/bar1.txt",
//     "foo/bar/bar2.txt"
//   ],
//   dirnames: [
//     "foo",
//     "foo/bar",
//     "foo/bar/baz"
//   ]
// }

const fs = require('fs');
const path = require('path');


function getDirsFiles(url, result = {filenames: [], dirnames: []}) {
  const objects = fs.readdirSync(url);

  result.dirnames.push(url);

  objects.forEach((obj) => {
    const link = `${url}/${obj}`;
    const isDirectory = fs.statSync(link).isDirectory();

    if (isDirectory) {
      getDirsFiles(link, result)
    } else {
      result.filenames.push(link);
    }
  })

  return result;
}

module.exports = getDirsFiles;

console.log(getDirsFiles('foo'));